package pt.webTransferAAS.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class webController {

	@RequestMapping("/login")
	public ModelAndView initController(HttpServletRequest request, HttpServletResponse response) {
		
		
		String username =request.getParameter("userName");
		String password =request.getParameter("password");
		String message;
		String auth = (String) request.getSession().getAttribute("Auth");
		if (username != null && !username .equals("") && username .equals("plory") && password != null
				&& !password.equals("") && password.equals("123")) {
			request.getSession().setAttribute("Auth", "true");
			message = "Bem vindo " + username + ".";
			
			return new ModelAndView("downloadList", 
		    		  "message", message); 

		} else if(!auth.isEmpty() && auth != null && auth.equals("true")){
			return new ModelAndView("downloadList","message", "");
		}else{
			message = "erro";
			return new ModelAndView("errorPage", "message", message);
		}

	}
	
	@RequestMapping("/lista")
	public ModelAndView listaPag(HttpServletRequest request, HttpServletResponse response) {
		String auth = (String) request.getSession().getAttribute("Auth");
		String message;
		if (!auth.isEmpty() && auth != null && auth.equals("true")) {
			return new ModelAndView("downloadList");
		} else {
			message = "erro";
			return new ModelAndView("index", "message", message);
		}

	}
	@RequestMapping("/upload")
	public ModelAndView uploadPag(HttpServletRequest request, HttpServletResponse response) {
		String auth = (String) request.getSession().getAttribute("Auth");
		String message;
		if (!auth.isEmpty() && auth.equals("true")) {
			return new ModelAndView("upload");
		} else {
			message = "erro";
			return new ModelAndView("index", "message", message);
		}

	}
	
	@RequestMapping("/conta")
	public ModelAndView contaPag(HttpServletRequest request, HttpServletResponse response) {
		String auth = (String) request.getSession().getAttribute("Auth");
		String message;
		if (!auth.isEmpty() && auth.equals("true")) {
			return new ModelAndView("perfil");
		} else {
			message = "erro";
			return new ModelAndView("index", "message", message);
		}

	}
	
}
