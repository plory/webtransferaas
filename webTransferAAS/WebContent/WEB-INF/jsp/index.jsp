<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<link rel="stylesheet"
	esheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
	integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
	crossorigin="anonymous">

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--====================================================================================================================================-->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<!--====================================================================================================================================-->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
<!--====================================================================================================================================-->
<title>Login</title>
</head>
<body>
	<!-- <div align="center"><a href="https://www.radarsystems.net/" target="_blank"><img src="https://i.imgur.com/OT5BZM4.jpg" width="555" height="285" class="img-fluid"></a></div> -->
	<!-- HEADER -->
	<header>

		 <jsp:include  flush="true" page="header.jsp"/>

	</header>


	<div class="container">
		<br> <br>
		<div class="row">
			<div class="col">
				<img src="https://i.imgur.com/87ZUO6G.png" width="450" height="450"
					class="Avatar">
			</div>
			<div class="col">

				<form class="login100-form validate-form" method=post action = "${pageContext.request.contextPath}/login" modelAttribute="user">
					<br> <br> <br>
					<h3 align="center">Login</h3>
					<br>

					<div class="container">
						<i class="fa fa-users"></i> <label >Username</label> 
						<input type="text" class="form-control" placeholder="Username" name="userName" />
						<br> <i class="fa fa-key"></i> <label for="">Password</label>
						<input type="password" name="password" class="form-control" placeholder="Password" />
					</div>
					<div>Username ou password incorreto</div>
					<br>
					<div class="col">
						<button class="btn btn-success btn-lg btn-block" value="login">
							<!--<i class="fas fa-share-square"></i>-->
							Entrar
						</button>
					</div>

				</form>
			</div>
		</div>

		<br>
	</div>
	<br>
	<br>
	<br>
	<br>

	<!--====================================================================================================================================-->
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
		integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
		crossorigin="anonymous"></script>
	<!--====================================================================================================================================-->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<!--====================================================================================================================================-->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
		integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
		crossorigin="anonymous"></script>
		
</html>